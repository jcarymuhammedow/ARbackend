from django.apps import AppConfig


class DolandyryshPaneliConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dolandyrysh_paneli'
