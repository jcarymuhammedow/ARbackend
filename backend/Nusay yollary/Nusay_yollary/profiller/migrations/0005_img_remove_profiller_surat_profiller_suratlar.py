# Generated by Django 4.2.7 on 2023-12-21 13:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiller', '0004_profiller_surat'),
    ]

    operations = [
        migrations.CreateModel(
            name='img',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('img', models.ImageField(default='', upload_to='static/img/profil')),
            ],
        ),
        migrations.RemoveField(
            model_name='profiller',
            name='Surat',
        ),
        migrations.AddField(
            model_name='profiller',
            name='Suratlar',
            field=models.ManyToManyField(related_name='profil_img', to='profiller.img'),
        ),
    ]
