# Generated by Django 4.2.7 on 2023-12-24 11:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiller', '0007_alter_profiller_mail'),
    ]

    operations = [
        migrations.AddField(
            model_name='profiller',
            name='Maglumat',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='profiller',
            name='Surat',
            field=models.ImageField(default='', upload_to='profiller'),
        ),
    ]
