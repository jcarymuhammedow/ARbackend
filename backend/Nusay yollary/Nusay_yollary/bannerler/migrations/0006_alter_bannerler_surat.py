# Generated by Django 4.2.7 on 2023-12-24 11:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bannerler', '0005_bannerler_tel_belgiler'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bannerler',
            name='Surat',
            field=models.ImageField(default='', upload_to='banner'),
        ),
    ]
