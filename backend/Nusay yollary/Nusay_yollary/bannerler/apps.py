from django.apps import AppConfig


class BannerlerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bannerler'
